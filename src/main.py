""" модуль для запуска тудушки """
import datetime
import logging

from todo import TodoJournal


def main():
    # инициализация логирования
    logging.basicConfig(filename='todo_journal.log', level=logging.INFO)

    # сообщение о начале логирования
    logging.info('Started')

    # создание json-файла с предусмотренной структурой
    TodoJournal.create('../data/todo.json', 'todo')

    # создание объекта класса
    todo = TodoJournal('../data/todo.json')

    # добавление новой записи с содержимым 'new example task'
    todo.add_entry("простая задача", 3, datetime.datetime(2024, 1, 15))
    todo.add_entry("задача чуть посложнее", 2, datetime.datetime(2024, 2, 1))
    todo.add_entry("очень трудная задача", 1, datetime.datetime(2024, 1, 10))
    todo.add_entry("просроченная задача", 1, datetime.datetime(2023, 12, 29))
    todo.add_entry("большая, долгая и скучная задача", 4, datetime.datetime(2024, 1, 7))

    # удаление ранее добавленной задачи
    # todo.remove_entry(0)

    todo.print()

    # сообщение об окончании логирования
    logging.info("Finished")


if __name__ == '__main__':
    main()
