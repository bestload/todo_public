""" модуль с реализацией класса тудушки """

import json
import logging
import pathlib
import sys
from datetime import datetime
from operator import itemgetter
from typing import Iterator


class TodoJournal:
    """ класс тудушки на базе json """

    def __init__(self, path_todo: str):
        """ конструктор тудушки """

        self.path_todo = path_todo
        self._parse()

    def __iter__(self) -> Iterator[str]:
        return iter(self.entries)

    @staticmethod
    def create(path_todo: str, name: str) -> None:
        """ метод создания тудушки

        :param path_todo: путь до json-файла с тудушкой
        :param name: имя(название) тудушки
        """

        print(list(pathlib.Path(path_todo).parents))

        if not pathlib.Path(path_todo).is_file():
            logging.error(f"create: Not found {path_todo}")

            print(f'Файла {path_todo} не существует или неверно указан путь до него! ')
            user_answer = input(
                'Создать несуществующие директории? (y/n): ')  # спрашиваем разрешение на создание новых директорий

            if user_answer.lower() == 'y':
                directories_to_create = list(pathlib.Path(path_todo).parents)
                for directory in reversed(directories_to_create):
                    directory.mkdir()
                    logging.info(f"Dir {directory} created.")

        if pathlib.Path(path_todo).is_file():
            with open(path_todo, "w", encoding='utf-8') as todo_file:
                json.dump(
                    {"name": name, "todos": []},
                    todo_file,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )

    def add_entry(self, task_label: str, task_priority: int, task_finish_date: datetime) -> None:
        """ метод добавления новой задачи(записи)

        :param task_label: содержание задачи
        :param task_priority: приоритет задачи
        :param task_finish_date: дедлайн задачи
        """

        self._parse()
        self.entries.append(
            {'label': task_label, 'priority': task_priority, 'deadline': json.dumps(task_finish_date, default=str)})

        logging.info("add new entry: {}".format(task_label))

        new_data = {
            "name": self.name,
            "todos": self.entries,
        }

        self._update(new_data)

    def remove_entry(self, index: int) -> None:
        """ метод удаления тудушки по индексу

        :param index: индекс удаляемой задачи
        """

        self.entries.remove(self.entries[index])

        new_data = {
            "name": self.name,
            "todos": self.entries,
        }

        self._update(new_data)

    def _update(self, changed_data: dict) -> None:
        """ метод обновления содержимого json-файла с тудушкой

        :param changed_data: обновленные словарь с именем объекта todo и списком задач
        """

        with open(self.path_todo, "w", encoding='utf-8') as todo_file:
            json.dump(
                changed_data,
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def _parse(self):
        """ метод считывания(парсинга) содержимого json-файла с тудушкой """

        try:
            with open(self.path_todo, 'r', encoding='utf-8') as todo_file:
                data = json.load(todo_file)

                self.name = data['name']
                self.entries = data['todos']
        except FileNotFoundError as error:
            print(f"{error}")
            print(f"Не существует такой тудушки: {self.path_todo}")
            sys.exit(1)

    def print(self) -> None:
        """
        Метод вывода содержащихся записей на экран

        :return: None
        """

        print("\t -- Start of Todo Tasks --")

        for i, entry in enumerate(self.entries):
            print(i + 1, '->', entry['label'])

        print("\t -- End of Todo Tasks --")
